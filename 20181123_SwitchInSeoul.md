# SwitchInSeoul - I

- Saturday Evening (?)

## prelist

| Nat. | username | Switch | Pro control | Joycon | Mario Kart | Overcooked 2 | Splatoon 2 | Towerfall |
| ---- | -------- | ------ | ----------- | ------ | ---------- | ------------ | ---------- | --------- |
| 🇫🇷    | wolfh    | ✔️      | 1          | 4      | ✔️        | ✔️            | ✔️          | ✔️         |
| 🇰🇷    | megabie  | ✔️      |            |        | ✔️        | ✔️            |            |           |
| 🇰🇷    | SooChan  | ✔️      |            |        |            |              |            |           |
| 🇰🇷    | megabieF |  ?       |            |        |            |              |            |           |
| 🇰🇷    | Steve    | ✔️      |            |        |            |              |            |           |
| 🇨🇦    | Douglas  | ✔️      |            |        |            |              |            |           |
| 🇺🇸    | Gabe     | ✔️      |            |        |            |              |            |           |
| 🇺🇸    | Isaac    | ✔️      |            |        |            |              |            |           |
| 🇫🇷    | Ainur    | ❌      |           |        |            |              |            |           |
| 🇫🇷 🇵🇹 | Piu      | ❌      |            |        |            |              |            |           |
| 🇰🇷    | Andy     | ❌      | housemate |        |            |              |            |           |
| 🇮🇹    | Paolo    | ❌      | housemate |        |             |             |             |          |
| 🇸🇬    | Huiyi    | ❌      | housemate |        |            |             |             |           |

## Place

- Trader (I should check the place)
- Home (if not possible at trader)

## Tournament

- Mario Kart
    - 2 per switch
- Towerfall (tournament rules)
    - Prewarming with 1v1v1v1v1v1
    - Tournament with 1v1v1v1 (and tournament rules)
    - Final with 1v1 (and tournament rules)